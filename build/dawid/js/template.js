'use strict';

// file: event.js

// end file: event.js

// file: forms.js
$(window).load(function () {

    $('.widget-type-form .hs-form-field').each(function () {
        var label = $(this).find('label').find('span').first();

        if (label.html() == '') {
            $(this).find('label').first().remove();
        } else if ($(this).find('.hs-form-required')) {
            $(label).addClass('required');
        }

        $(this).find('select').addClass('select');
    });
});

// Functions for dropdown
function select(el, currentElement) {
    $(currentElement).each(function () {
        $(this).find("select").each(function () {
            var parent = $(this).parent();
            parent.addClass('dropdown_select');
            parent.append('<div class="dropdown-header"><span>-Select-</span></div>');
            parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
            $(this).find('option').each(function () {
                if ($(this).val() != "") {
                    parent.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).val() + '</li>');
                }
            });
        });
    });
    $(currentElement).find('.dropdown_select.input .dropdown-header').click(function (event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $(currentElement).find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });
}

waitForLoad(".form, .widget-type-form,.widget-type-blog_content", "form", select);
// end file: forms.js

// file: global.js
$(document).ready(function () {

    footerScrollUp();
    waitForLoad();
});

// ***************************************************


function footerScrollUp() {
    $(".btn-go-top").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 800);
        return false;
    });
}

// ***************************************************


function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}

waitForLoad(".form, .widget-type-form, .widget-type-blog_content", "form", moveButton);
waitForLoad(".form, .widget-type-form, .widget-type-blog_content", "form", wrapInputs);
waitForLoad(".form, .widget-type-form, .widget-type-blog_content", ".hs_recaptcha", moveCaptcha);

// end file: global.js

// file: menu.js
$(document).ready(function () {
    headerSearchInit();
    menuMobileInit();
    activeMobileMenu();
    menuLayout();
});

$(window).resize(function () {

    // headerSearchInit();
    menuMobileInit();
    activeMobileMenu();
});

$(document).ready(function () {

    if ($('body.naed')) {

        var iScrollPos = 100;

        $(window).scroll(function () {

            var iCurScrollPos = $(this).scrollTop();
            if (iCurScrollPos > iScrollPos && iScrollPos > 0) {
                $('.header-top').addClass('scroll');
                $('.header').addClass('scroll');
            } else {

                $('.header-top').removeClass('scroll');
                $('.header').removeClass('scroll');
            }
            iScrollPos = iCurScrollPos;
        });
    }
});

function menuMobileInit() {
    $('header.header .menu-container .menu-wrapper span div>ul').slicknav({
        prependTo: 'header.header .wrapper > div',
        label: "",
        duration: 500,
        init: function init() {
            $("header.header--main .slicknav_menu .slicknav_nav>li:first").before("<li class='search'></li>");
            var search = $(".google-search-module-popup .google-search-module-form").clone(true);
            $("header.header--main .slicknav_menu .slicknav_nav li.search").append(search);
        }
    });

    $('.header .slicknav_menu .slicknav_btn').on('click', function () {
        $('header.header--main .slicknav_menu .slicknav_nav').toggleClass('open');

        var menuHeight = $('header.header').innerHeight();
        $('body').css("overflow", "hidden");
        if ($('.slicknav_nav').hasClass('open')) {

            $('ul.slicknav_nav').css('top', menuHeight + 'px');
            $('ul.slicknav_nav > li:last-child').css('marginBottom', menuHeight);
        } else {
            $('body').css("overflow", "auto");
        }
    });
}

function menuLayout() {
    if ($('body.naed').length != 1) {
        $('.menu-container .menu-wrapper ul li.hs-item-has-children').not('ul li ul li').each(function () {

            var menuWidth = $(this).innerWidth();
            var submenuWidth = $(this).find('.hs-menu-depth-2').innerWidth();

            if (menuWidth > submenuWidth) {
                $(this).find('.hs-menu-children-wrapper').css('width', '100%');
                $(this).find('.hs-menu-depth-3').parent().css('width', 'auto');
            } else {
                $(this).css("width", submenuWidth);
                $(this).find('.hs-menu-children-wrapper').css('width', '100%');
                $(this).find('.hs-menu-depth-3').parent().css('width', 'auto');
            }
        });
    }

    $('.hs-menu-depth-2 .hs-menu-children-wrapper:not(.slicknav_hidden)').each(function () {

        if ($(this).find('li').length == 1) {
            console.log(this);
            $(this).addClass('one-element');
        }
    });
}

function activeMobileMenu() {
    if (parseInt($(".header.header .menu-container").data("mobile-active")) >= window.innerWidth) {
        $(".header.header").addClass("mobile");
        return;
    } else {
        $(".header.header").removeClass("mobile");
    }
}

function headerSearchInit() {
    $(" .google-search-module-btn").click(function () {
        if (!$(" .google-search-module-popup").hasClass("show-popup")) $(".google-search-module-popup").addClass("show-popup");else $(".google-search-module-popup").removeClass("show-popup");
    });
    $(" .google-search-module-popup-close").click(function () {
        if ($(".google-search-module-popup").hasClass("show-popup")) $(".google-search-module-popup").removeClass("show-popup");
    });
    $(".google-search-module-popup").click(function (e) {
        e.stopPropagation();
        if ($(" .google-search-module-popup").hasClass("show-popup")) $(" .google-search-module-popup").removeClass("show-popup");
    });
    $(".google-search-module-form,.google-search-module-icon, .google-search-module-label").click(function (e) {
        e.stopPropagation();
    });
    $(" .google-search-module-icon").click(function () {
        if ($(" .google-search-module-popup form a").length > 0) $(" .google-search-module-popup form a").click();else $(" #google-custom-search").submit();
    });
}
// end file: menu.js

// file: Blog/blog.js

function moveButton() {

    $("#comments-listing").find(".comment").each(function () {

        var date = $(this).children(".comment-date");
        var button = $(this).children(".comment-reply-to").html("- <span>Reply</span>");

        button.appendTo(date);
    });
}

function wrapInputs() {

    var form = $("#comment-form > form"),
        columnLeft = form.children("div").not(".hs_comment, .hs_lifecyclestage, .hs_submit, .hs_recaptcha"),
        columnRight,
        wrapperLeft = "<div class='form-left' />",
        wrapperRight = "<div class='form-right' />",
        header = "<h4>Leave comment:</h4>";

    columnLeft.wrapAll(wrapperLeft);

    columnRight = form.children().not(".form-left");

    columnRight.wrapAll(wrapperRight);

    form.before(header);

    form.find(".hs-form-field").find("label, legend").remove();
}

function moveCaptcha() {
    $('.hs_recaptcha').insertAfter($('.form-right .hs_comment'));
}

// end file: Blog/blog.js

// file: Modules/3blog-post-section.js
$(document).ready(function () {

    var windowWidth = $(window).innerWidth();

    if ($('.po-related-post-module__slider > div').length > 3 || windowWidth < 991) {
        $('.po-related-post-module__slider').not('.slick-initialized').slick({
            mobileFirst: true,
            infinite: true,
            autoplay: true,
            arrows: false,
            dots: true,
            dotsClass: 'va-slick-dots',
            slidesToShow: 1,
            responsive: [{
                breakpoint: 991,
                settings: {
                    slidesToShow: 3,
                    autoplay: false,
                    arrows: true,
                    dots: false
                }
            }]
        });

        /* Unslick fix */
        $(window).resize(function () {
            $('.po-related-post-module__slider').slick('resize');
        });

        $(window).on('orientationchange', function () {
            $('.po-related-post-module__slider').slick('resize');
        });
    }
});

// end file: Modules/3blog-post-section.js

// file: Modules/po-hero-banner.js
$(document).ready(function () {
    mouseWhellScroll();
});
function mouseWhellScroll() {

    var scrollTo = $('.hero-banner-inner').height() + 50;

    $(".po-hero-banner .mouse").click(function () {
        $("html, body").animate({
            scrollTop: scrollTo
        }, 800);
        return false;
    });
}

// end file: Modules/po-hero-banner.js

// file: Modules/po-naed-calendar-feed.js
function initCalendarFeed(googleApiKey, calendarId, days) {
    var $fc = $('#naed-calendar-feed');

    var options = {
        googleCalendarApiKey: googleApiKey,
        footer: false,
        header: {
            left: '',
            right: 'prev,next',
            center: ''
        },
        events: {
            googleCalendarId: calendarId,
            className: 'naed-event-feed'
        },
        timeFormat: 'H(:mm)',
        displayEventEnd: true,
        views: {
            fullList: {
                type: 'list',
                duration: { days: days }
            }
        },
        defaultView: 'fullList',
        eventRender: function eventRender(event, element) {

            element.find('.fc-list-item-marker').remove();
            if (typeof event.description != 'undefined') {
                var eventDescription = truncate(event.description, 30);
                setTimeout(function () {
                    element.after('<tr class="details-row"><td colspan="2" class="naed-event-feed__details">' + eventDescription + '</td></tr>');
                });
            }
        }
    };

    $fc.fullCalendar(options);
}

function truncate(str, no_words) {
    if (str.split(" ").length > no_words) {
        return str.split(" ").splice(0, no_words).join(" ") + '...';
    } else {
        return str;
    }
}
// end file: Modules/po-naed-calendar-feed.js

// file: Modules/po-naed-calendar.js
function initCalendar(googleApiKey, calendarId) {
    var $fc = $('#naed-calendar');

    var options = {
        googleCalendarApiKey: googleApiKey,
        header: {
            left: 'title',
            center: '',
            right: 'today, prev, next'
        },
        events: {
            googleCalendarId: calendarId,
            className: 'naed-event'
        },
        timeFormat: 'H(:mm)',
        displayEventEnd: true,
        defaultView: 'month',
        eventMouseover: function eventMouseover(event, jsEvent, view) {

            var thisElem = $(this);

            var eventStart = event.start.format('dddd, MMMM dS, HH:MM');

            if (typeof event.title != 'undefined') {
                var eventTitle = '<span class="details-title">' + event.title + '</span>';
            } else {
                var eventTitle = '';
            }

            if (typeof event.description != 'undefined') {
                var eventDescription = '<span class="details-summary">' + truncate(event.description, 30) + '</span>';
            } else {
                var eventDescription = '';
            }

            $(this).parents('.fc-row').css('z-index', '10');

            if ($(this).find('.naed-event__details').length == 0) {
                $(this).append('<div class="naed-event__details hide">' + '<span class="details-start">' + eventStart + '</span>' + eventTitle + eventDescription + '</div>');
            }

            $('.naed-event__details').addClass('hide');

            $(this).find('.naed-event__details').removeClass('hide');
        },
        eventMouseout: function eventMouseout(event, jsEvent, view) {
            $(this).find('.naed-event__details').addClass('hide');

            setTimeout(function () {
                $(this).parents('.fc-row').css('z-index', '');
            }, 300);
        }

    };

    $fc.fullCalendar(options);
    $(window).resize();
}

$(window).resize(function () {
    if ($(window).width() < 991) {
        $('#naed-calendar').fullCalendar('changeView', 'listMonth');
    } else {
        $('#naed-calendar').fullCalendar('changeView', 'month');
    }
});

function truncate(str, no_words) {
    if (str.split(" ").length > no_words) {
        return str.split(" ").splice(0, no_words).join(" ") + '...';
    } else {
        return str;
    }
}
// end file: Modules/po-naed-calendar.js

// file: Modules/po-slider.js
$(document).ready(function () {

    $(".po-slider .hs_cos_wrapper_type_widget_container").not(".slick-initialized").slick({
        dots: false,
        infinite: true,
        dotsClass: "slider-dots",
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: "30%",
        variableWidth: true,
        variableHeight: true,
        initialSlide: findMiddleSlide(".po-slider .slide"),
        adaptiveHeight: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                centerPadding: "0px",
                dots: true,
                arrows: false
            }
        }, {
            breakpoint: 431,
            settings: {
                centerPadding: "0px",
                variableWidth: false,
                dots: true,
                arrows: false
            }
        }]
    });

    // po-related-product

});

function findMiddleSlide(a) {
    var c, b;
    c = $(a).length;
    if (c > 2) {
        if (c % 2 == 0) {
            b = Math.floor(c / 2) - 1;
        } else {
            b = Math.floor(c / 2);
        }
    } else {
        b = 0;
    }
    return b;
}

// end file: Modules/po-slider.js

// file: Modules/po-testimonial-slider.js

$(document).ready(function () {

    var testimonialSlider = $('.po-testimonial-slider > span');
    var initialSlide = 1;

    testimonialSlider.on('init', function (slick) {
        var items = $(this).children().length;
        if (items === 1) {
            $('.slick-slide').addClass('slick-one');
        }
        var dots = testimonialSlider.find("div.slick-current .slick-dots li");
        dots.each(function () {
            $(this).removeClass("slick-active");
        });
        $(dots[initialSlide]).addClass("slick-color");
        $(this).find(".slick-current").prev().addClass("slick-before");
    });

    testimonialSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        $(this).find(".slick-slide").removeClass("slick-before");
        $(slick.$slides.get(nextSlide)).prev().addClass("slick-before");
    });

    testimonialSlider.on('afterChange', function (event, slick, currentSlide, nextSlide) {
        var dots = testimonialSlider.find("div.slick-current .slick-dots li");
        var index = $(slick.$slides.get(currentSlide)).data('slick-index');
        dots.each(function () {
            $(this).removeClass("slick-active");
        });
        $(dots[index]).addClass("slick-color");
    });

    testimonialSlider.slick({
        dots: true,
        appendDots: $('.pagination'),
        initialSlide: initialSlide,
        infinite: false,
        centerMode: true,
        slidesToShow: 1,
        slidesToScroll: -1,
        arrows: true,
        accessibility: false,
        touchMove: false,
        variableWidth: true,
        speed: 500,
        nextArrow: '<button type="button" class="prev"><img src="https://cdn2.hubspot.net/hubfs/685080/Potassium/right.png"></button>',
        prevArrow: '<button type="button" class="next"><img src="https://cdn2.hubspot.net/hubfs/685080/Potassium/left.png"></button>'
    });
});

// end file: Modules/po-testimonial-slider.js

// file: Website/home.js
$(document).ready(function () {

    skillsInit();
});

function skillsInit() {
    var checkBars = 0;
    $(window).scroll(function () {
        var scrollTop = $(window).scrollTop();
        if ($(".po-circle-progress-wrapper").length > 0) {
            loadingBars();
        }
    });

    $(window).scroll();

    function loadingBars() {
        var scrollTop = $(window).scrollTop();
        var positionBars = $(".po-circle-progress-wrapper").offset().top;
        var windowHeight = $(window).height();
        var scrollBottom = scrollTop + windowHeight;
        if (scrollBottom > positionBars && checkBars == 0) {
            checkBars = 1;
            $(".po-circle-progress .circle").circliful({
                animation: 1,
                multiPercentage: 1,
                animationStep: 5,
                foregroundBorderWidth: 10,
                backgroundBorderWidth: 1
            });
            $(".po-circle-progress .circle .timer").attr('y', '115');
        }
    };
}
// end file: Website/home.js
//# sourceMappingURL=template.js.map
