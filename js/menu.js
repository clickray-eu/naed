$(document).ready(function() {
    headerSearchInit();
    menuMobileInit();
    activeMobileMenu();
    menuLayout();
});


$(window).resize(function() {
   
    // headerSearchInit();
    menuMobileInit();
    activeMobileMenu();

});

$(document).ready(function() {

if( $('body.naed') ){

    var iScrollPos = 100;
  
    $(window).scroll(function () {
        
        var iCurScrollPos = $(this).scrollTop();
        if (iCurScrollPos > iScrollPos && iScrollPos > 0) {
           $('.header-top').addClass('scroll');
           $('.header').addClass('scroll');

        } else {

           $('.header-top').removeClass('scroll');
           $('.header').removeClass('scroll');

        }
        iScrollPos = iCurScrollPos;

    });

}

});

function menuMobileInit() {
    $('header.header .menu-container .menu-wrapper span div>ul').slicknav({
        prependTo: 'header.header .wrapper > div',
        label: "",
        duration: 500,
        init: function() {
            $("header.header--main .slicknav_menu .slicknav_nav>li:first").before("<li class='search'></li>");
            var search = $(".google-search-module-popup .google-search-module-form").clone(true);
            var headerTop = $('.header-top ul li').clone(true).addClass('header-top--item');
            $("header.header--main .slicknav_menu .slicknav_nav li.search").append(search);
            $('header.header--main .slicknav_menu .slicknav_nav').append(headerTop);
        },
    });

    $('.header .slicknav_menu .slicknav_btn').on('click', function() {
        $('header.header--main .slicknav_menu .slicknav_nav').toggleClass('open');

        var menuHeight = $('header.header').innerHeight();
$('body').css("overflow", "hidden");
        if ($('.slicknav_nav').hasClass('open')) {

            
            $('ul.slicknav_nav').css('top', menuHeight + 'px');
            $('ul.slicknav_nav > li:last-child').css('marginBottom', menuHeight );

        } else {
            $('body').css("overflow", "auto");

        }
    })
}

function menuLayout() {
    if( $('body.naed').length != 1 ){
        $('.menu-container .menu-wrapper ul li.hs-item-has-children').not('ul li ul li').each(function() {

            var menuWidth = $(this).innerWidth();
            var submenuWidth = $(this).find('.hs-menu-depth-2').innerWidth();

            if (menuWidth > submenuWidth) {
                $(this).find('.hs-menu-children-wrapper').css('width', '100%');
                $(this).find('.hs-menu-depth-3').parent().css('width', 'auto');
            } else {
                $(this).css("width", submenuWidth);
                $(this).find('.hs-menu-children-wrapper').css('width', '100%');
                $(this).find('.hs-menu-depth-3').parent().css('width', 'auto');


            }


        })
    }

    $('.hs-menu-depth-2 .hs-menu-children-wrapper:not(.slicknav_hidden)').each(function(){

            if( $(this).find('li').length == 1 ){
                $(this).addClass('one-element');    
             }

        })
}

function activeMobileMenu() {
    if (parseInt($(".header.header .menu-container").data("mobile-active")) >= window.innerWidth) {
        $(".header.header").addClass("mobile");
        return;
    } else {
        $(".header.header").removeClass("mobile");

    }
}

function headerSearchInit() {
    $(".google-search-module-btn").click(function() {
        if (!$(" .google-search-module-popup").hasClass("show-popup"))
            $(".google-search-module-popup").addClass("show-popup")
        else
            $(".google-search-module-popup").removeClass("show-popup")

    });
    $(" .google-search-module-popup-close").click(function() {
        if ($(".google-search-module-popup").hasClass("show-popup"))
            $(".google-search-module-popup").removeClass("show-popup");

    });
    $(".google-search-module-popup").click(function(e) {
        e.stopPropagation();
        if ($(" .google-search-module-popup").hasClass("show-popup"))
            $(" .google-search-module-popup").removeClass("show-popup");
    });
    $(".google-search-module-form,.google-search-module-icon, .google-search-module-label").click(function(e) {
        e.stopPropagation();
    });
    $(" .google-search-module-icon").click(function() {
        if ($(" .google-search-module-popup form a").length > 0)
            $(" .google-search-module-popup form a").click();
        else
            $(" #google-custom-search").submit();
    });
}