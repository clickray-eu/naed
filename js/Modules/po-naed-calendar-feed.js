function initCalendarFeed(googleApiKey, calendarId, days) {
    var $fc = $('#naed-calendar-feed')

    var options = {
        googleCalendarApiKey: googleApiKey,
        noEventsMessage: 'No events this week',
        footer: false,
        header: {
          left: '',
          right: 'prev,next',
          center: ''
        },
        events: {
            googleCalendarId: calendarId,
            className: 'naed-event-feed'
        },
        timeFormat: 'H(:mm)',
        displayEventEnd: true,
        views: {
          fullList: {
            type: 'list',
            duration: { days: days }
          }
        },
        defaultView: 'fullList',
        eventRender: function(event, element) {
          
          element.find('.fc-list-item-marker').remove();
          if (typeof event.description != 'undefined') {
            var eventDescription = truncate(event.description, 30);
            setTimeout(function() {
              element.after('<tr class="details-row"><td colspan="2" class="naed-event-feed__details">' + eventDescription + '</td></tr>'); 
            });
          }
        }
    }

    $fc.fullCalendar(options);
}


function truncate(str, no_words) {
    if (str.split(" ").length > no_words) {
      return str.split(" ").splice(0,no_words).join(" ") + '...';
    } else {
      return str;
    }
}