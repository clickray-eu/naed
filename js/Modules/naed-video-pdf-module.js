// hoover effect
$('.naed-video-box').on('mouseenter', function() {
	var moduleHeight = $(this).outerHeight();
	var descriptionHeight = $(this).find('.content-wrapper').outerHeight() + 15;
	var titlePosition = (moduleHeight - descriptionHeight)/4;

	$(this).find('.title-wrapper').css('top', titlePosition + 'px');
});

$('.naed-video-box').hover('mouseleave', function() {
	$(this).find('.title-wrapper').removeAttr('style');
});

// magnific popup
$(document).ready(function() {
	$('.naed-video-box__play-button').magnificPopup({
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
});