$(document).ready(function() {


 var windowWidth = $(window).innerWidth();

 if( $('.po-related-post-module__slider > div').length > 3 || windowWidth < 991){
	$('.po-related-post-module__slider').not('.slick-initialized').slick({
	  mobileFirst: true,
	  infinite: true,
	  autoplay: true,
	  arrows: false,
	  dots: true,
	  dotsClass: 'va-slick-dots',
	  slidesToShow: 1,
	  responsive: [
	    {
	      breakpoint: 991,
	      settings: {
	      	  slidesToShow: 3,
	      	  autoplay: false,
	      	  arrows: true,
	 		  dots: false
	      }
	    }
	  ]
	});

	/* Unslick fix */
	$(window).resize(function() {
	  $('.po-related-post-module__slider').slick('resize');
	});

	$(window).on('orientationchange', function() {
	  $('.po-related-post-module__slider').slick('resize');
	});
}

})
