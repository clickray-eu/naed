
$(document).ready(function () {

  var testimonialSlider = $('.po-testimonial-slider > span');
  var initialSlide = 1;

  testimonialSlider.on('init', function(slick) {
    var items = $(this).children().length;
    if (items === 1) { $('.slick-slide').addClass('slick-one');}
    var dots = testimonialSlider.find("div.slick-current .slick-dots li");
    dots.each(function() { $(this).removeClass("slick-active");})
    $(dots[initialSlide]).addClass("slick-color");
    $(this).find(".slick-current").prev().addClass("slick-before");
  })

  testimonialSlider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
    $(this).find(".slick-slide").removeClass("slick-before");
    $(slick.$slides.get(nextSlide)).prev().addClass("slick-before");
  });

  testimonialSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
    var dots = testimonialSlider.find("div.slick-current .slick-dots li");
    var index = $(slick.$slides.get(currentSlide)).data('slick-index');
    dots.each(function() { $(this).removeClass("slick-active"); })
    $(dots[index]).addClass("slick-color");  
  });

  testimonialSlider.slick({
    dots: true,
    appendDots: $('.pagination'),
    initialSlide: initialSlide,
    infinite: false,
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: -1,
    arrows: true,
    accessibility: false,
    touchMove: false,
    variableWidth: true,
    speed: 500,
    nextArrow: '<button type="button" class="prev"><img src="https://cdn2.hubspot.net/hubfs/685080/Potassium/right.png"></button>',
    prevArrow: '<button type="button" class="next"><img src="https://cdn2.hubspot.net/hubfs/685080/Potassium/left.png"></button>'
  });


});
