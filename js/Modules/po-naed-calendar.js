function initCalendar(googleApiKey, calendarId) {
    var $fc = $('#naed-calendar')

    var options = {
        googleCalendarApiKey: googleApiKey,
        header: {
          left: 'title',
          center: '',
          right: 'today, prev, next'
        },
        events: {
            googleCalendarId: calendarId,
            className: 'naed-event'
        },
        timeFormat: 'H(:mm)',
        displayEventEnd: true,
        defaultView: 'month',
        eventMouseover: function(event, jsEvent, view) {
          
            if (event.allDay == false) {
                var eventStart = event.start.format('dddd, MMMM Do, hh:mm');
            } else {
                var eventStart = event.start.format('dddd, MMMM Do');
            }
          
          if (typeof event.title != 'undefined') {
            var eventTitle = '<span class="details-title">' + event.title + '</span>'
          } else {
            var eventTitle = '';
          }
          
          if (typeof event.description != 'undefined') {
            var eventDescription = '<span class="details-summary">' + truncate(event.description, 30) + '</span>';
          } else {
            var eventDescription = '';
          }

          $('#naed-calendar .fc-row').css('z-index', '');
          $(this).parents('.fc-row').css('z-index', '10');

          if ($(this).find('.naed-event__details').length == 0) {
            $(this).append('<div class="naed-event__details hide">' + '<span class="details-start">' + eventStart + '</span>' + eventTitle + eventDescription + '</div>');
          }

          $('.naed-event__details').addClass('hide');

          $(this).find('.naed-event__details').removeClass('hide');
          
          
        },
        eventMouseout: function(event, jsEvent, view) {
          $(this).find('.naed-event__details').addClass('hide');
          
          setTimeout(function(){
              $(this).parents('.fc-row').css('z-index', '');
          }, 300);
        }

    }

    $fc.fullCalendar(options);
    $(window).resize();
}


$(window).resize(function() {
  if ($(window).width() < 991) {
    $('#naed-calendar').fullCalendar('changeView', 'listMonth');
  } else {
    $('#naed-calendar').fullCalendar('changeView', 'month');
  }
});

function truncate(str, no_words) {
    if (str.split(" ").length > no_words) {
      return str.split(" ").splice(0,no_words).join(" ") + '...';
    } else {
      return str;
    }
}