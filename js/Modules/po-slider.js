$(document).ready(function () {


    $(".po-slider .hs_cos_wrapper_type_widget_container").not(".slick-initialized").slick({
        dots: false,
        infinite: true,
        dotsClass: "slider-dots",
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        centerMode: true,
        centerPadding: "30%",
        variableWidth: true,
        variableHeight: true,
        initialSlide: findMiddleSlide(".po-slider .slide"),
        adaptiveHeight: true,
        responsive: [{
            breakpoint: 768,
            settings: {
                centerPadding: "0px",
                dots: true,
                arrows: false
            }
        }, {
            breakpoint: 431,
            settings: {       
                centerPadding: "0px",
                variableWidth: false,
                dots: true,
                arrows: false
            }
        }]
    })


  // po-related-product



});

function findMiddleSlide(a) {
    var c, b;
    c = $(a).length;
    if (c > 2) {
        if (c % 2 == 0) {
            b = Math.floor(c / 2) - 1
        } else {
            b = Math.floor(c / 2)
        }
    } else {
        b = 0
    }
    return b
}
