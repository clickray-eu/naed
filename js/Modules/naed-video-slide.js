$(document).ready(function() {
	videoSliderInit();

    function videoSliderInit(){

        if ($('.naed-video-slider .naed-video-slide').length > 0) {
            $('.naed-video-slider').parent().parent().append('<div class="naed-video-desc-slider"></div>')

            $('.naed-video-slide .description').each(function(){
              $(this).appendTo('.naed-video-desc-slider');
            })

            $('.naed-video-slider .widget-type-widget_container > span').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                // autoplay: true,
                asNavFor: '.naed-video-desc-slider',
                arrows: true,
                dots: true, 
                centerMode: true,
                centerPadding: "33%",
                initialSlide: findMiddleSlide('.naed-video-slider .widget-type-widget_container > span'),
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            centerMode: false,
                            autoplay: true,
                            arrows: false,
                            slidesToScroll: 1
                        }
                    }
                ]
            });

            $('.naed-video-desc-slider').slick({
                slidesToShow: 1,
                centerMode: true,
                centerPadding: 0,
                arrows: false,
                dots: false,
                slidesToScroll: 1,
                draggable: false,
                asNavFor: '.naed-video-slider .widget-type-widget_container > span',
                initialSlide: findMiddleSlide('.naed-video-slider .widget-type-widget_container > span')
            });

            slickLoopTransformFix('.naed-video-slider .widget-type-widget_container > span', 'fake-current');
        }
    }


})

function findMiddleSlide(slideClass) {
    var slidesNum, initialSlide;
    slidesNum = $(slideClass).length;
    if (slidesNum > 2) {
        if (slidesNum % 2 == 0) {
            initialSlide = Math.floor(slidesNum / 2) - 1;
        } else {
            initialSlide = Math.floor(slidesNum / 2) ;
        }
    } else {
        initialSlide = 0;
    }
    return initialSlide;
}

function slickLoopTransformFix(sliderClass, fakeCurrentSlideClass) {
  var slider = $(sliderClass);
  var slidesCount = slider.find('.slick-slide:not(.slick-cloned)').length;
  slider.on('beforeChange', function(event, slick, currentSlide, nextSlide){
    // when on last slide and next is first
    slider.find('.slick-slide').removeClass(fakeCurrentSlideClass);
    if (currentSlide === (slidesCount - 1) && nextSlide === 0) {
      slider.find('.slick-slide[data-slick-index=' + String(slidesCount) + ']').addClass(fakeCurrentSlideClass);
    }
    // when on first slide and next is last
    if (currentSlide === 0 && nextSlide === slidesCount - 1) {
      slider.find('.slick-slide[data-slick-index=-1]').addClass(fakeCurrentSlideClass);
    }
  });
}