$( document ).ready(function() {

footerScrollUp();
waitForLoad();

});

// ***************************************************


function footerScrollUp() {
    $(".btn-go-top").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 800);
        return false;
    });
}


// ***************************************************


function waitForLoad(wrapper, element, callback) {
    if ($(wrapper).length > 0) {
        $(wrapper).each(function (i, el) {
            var waitForLoad = setInterval(function () {
                if ($(el).length == $(el).find(element).length) {
                    clearInterval(waitForLoad);
                    callback($(el), $(el).find(element));
                }
            }, 50);
        });
    }
}


waitForLoad(".form, .widget-type-form, .widget-type-blog_content", "form", moveButton);
waitForLoad(".form, .widget-type-form, .widget-type-blog_content", "form", wrapInputs);
waitForLoad(".form, .widget-type-form, .widget-type-blog_content", ".hs_recaptcha", moveCaptcha);

if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
    $("#cssmenu > ul > li > a").bind('touchstart', function(){
        console.log("touch started");
    });
};