$( document ).ready(function() {

 skillsInit()

});

function skillsInit() {
    var checkBars = 0;
    $(window).scroll(function() {
        var scrollTop = $(window).scrollTop();
        if ($(".po-circle-progress-wrapper").length > 0) {
            loadingBars();
        }
    });

    $(window).scroll();

    function loadingBars() {
        var scrollTop = $(window).scrollTop();
        var positionBars = $(".po-circle-progress-wrapper").offset().top;
        var windowHeight = $(window).height()
        var scrollBottom = scrollTop + windowHeight;
        if (scrollBottom  > positionBars && checkBars == 0) {
            checkBars = 1;
            $(".po-circle-progress .circle").circliful({
                animation: 1,
                multiPercentage: 1,
                animationStep: 5,
                foregroundBorderWidth: 10,
                backgroundBorderWidth: 1
            });
            $(".po-circle-progress .circle .timer").attr('y', '115');
        }
    };
}