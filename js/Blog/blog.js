
function moveButton() {

    $("#comments-listing").find(".comment").each(function() {

        var date = $(this).children(".comment-date");
		var button = $(this).children(".comment-reply-to").html("- <span>Reply</span>");
        
		button.appendTo(date);
        
    })
}

function wrapInputs() {

    var form = $("#comment-form > form"),
        columnLeft = form.children("div").not(".hs_comment, .hs_lifecyclestage, .hs_submit, .hs_recaptcha"),
        columnRight,
        wrapperLeft = "<div class='form-left' />",
        wrapperRight = "<div class='form-right' />",
        header = "<h4>Leave comment:</h4>";

    columnLeft.wrapAll(wrapperLeft);
    
    columnRight = form.children().not(".form-left");

    columnRight.wrapAll(wrapperRight);

    form.before(header);
    
    form.find(".hs-form-field").find("label, legend").remove();
}

function moveCaptcha() {
     $('.hs_recaptcha').insertAfter($('.form-right .hs_comment'));
}
