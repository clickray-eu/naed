$(window).load(function() {

	// $('.widget-type-form .hs-form-field').each(function() {
	// 	var label = $(this).find('label').find('span').first();
		
	// 	if (label.html() == '') {
	// 		$(this).find('label').first().remove();
	// 	} else if ($(this).find('.hs-form-required')) {
	// 		$(label).addClass('required');
	// 	}

	// 	$(this).find('select').addClass('select');
	// })

})


// Functions for dropdown
function select($wrapper,$form) {
    $wrapper.each(function () {
        $(this).find("select").each(function () {
            var parent = $(this).parent();
            parent.addClass('dropdown_select');
            parent.append('<div class="dropdown-header"><span>-Select-</span></div>');
            parent.append('<ul class="dropdown-list" style="display: none;"></ul>');
            $(this).find('option').each(function () {
                if ($(this).val() != "") {
                    parent.find("ul.dropdown-list").append('<li value="' + $(this).val() + '">' + $(this).val() + '</li>')
                }
            })
        })
    });
    $wrapper.find('.dropdown_select.input .dropdown-header').click(function (event) {
        $(this).toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).children('.arrow-white').toggle();
    });
    $wrapper.find('.dropdown-list li').click(function () {
        var choose = $(this).text();
        $(this).parent().siblings('.dropdown-header').find('span').text(choose);
        $(this).parent().siblings('select').find('option').removeAttr('selected');
        $(this).parent().siblings('select').val(choose).find(' option[value="' + choose + '"] ').attr('selected', 'selected').change();
        $(this).parent().find('li').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('.dropdown-header').toggleClass('slide-down').siblings('.dropdown-list').slideToggle();
        $(this).parent().siblings('.dropdown-header').children('.arrow-white').toggle();
    });
    $wrapper.find('.dropdown_select .input').click(function (event) {
        event.stopPropagation();
    });

}

waitForLoad(".hs_cos_wrapper_type_form, .widget-type-blog_content", "form", select);